import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "popups"

Rectangle {
    property var locale: Qt.locale()

    property int editingItemId: -1
    property var editingData: editingItemId !== -1 ? taskModel.getVariantItem(
                                                         editingItemId) : null
    property var defaultData: taskModel.getVariantDefaultItem()
	property var modelData: null
    property bool isProject: false
    signal onOpenedChanged(bool isOpened)

    function openEdit(itemId, modelData_) {
        editingItemId = itemId
		modelData = modelData_
        visible = true
        onOpenedChanged(true)
    }

    function close() {
        editingItemId = -1
        visible = false
        onOpenedChanged(false)
    }

    MouseArea {
        anchors.fill: parent
    }

    visible: false
    color: "#3b4252"
    GridLayout {
        id: contentLayout
        anchors.fill: parent
        anchors.margins: 10 * ratio
        columns: 2
        
        Button {
            Layout.preferredHeight: 60 * ratio
            Layout.preferredWidth: Layout.preferredHeight
            background: Rectangle {
                color: "transparent"
            }
            icon.source: modelData.isProject ? "qrc:/Icons/list.png" : "qrc:/Icons/radio_btn.png"
            icon.color: "#81a1c1"
            icon.height: height
            icon.width: height

            Button {
                visible: !modelData.isProject
                id: buttonCheckMark
                background: Rectangle {
                    color: "transparent"
                }
                anchors.fill: parent
                icon.source: "qrc:/Icons/ic_done_black_24dp.png"
                icon.color: "#eceff4"
                icon.height: height
                icon.width: height
                scale: modelData.isDone ? 1 : 0
            }

            Button {
                visible: !modelData.isProject
                checkable: true
                checked: modelData.isDone
                anchors.fill: parent
                background: Rectangle {
                    color: "transparent"
                }
                onClicked: {
                    modelData.isDone = checked
                    completeAnimation.from = checked ? 0 : 1
                    completeAnimation.to = checked ? 1 : 0
                    completeAnimation.start()
                }
                PropertyAnimation {
                    id: completeAnimation
                    target: buttonCheckMark
                    property: "scale"
                    duration: 100
                    easing.type: Easing.InSine
                }
            }
        }

        TextField {
            id: taskName
            property bool isInvalidated: false
            Layout.fillWidth: true
            placeholderText: "Add task..."
            text: modelData.name
            font.pixelSize: 30 * ratio
            background: Rectangle {
                color: "#d8dee9"
                radius: height / 4
            }
            function isFilledCheck() {
                if (text === "") {
                    background.color = "#bf616a"
                    isInvalidated = true
                    return false
                }
                return true
            }
            function reset() {
                taskName.background.color = "#d8dee9"
            }
            onTextChanged: {
                if (isInvalidated && text !== "") {
                    isInvalidated = false
                    reset()
                }
            }
        }
        Text {
            text: "Start date"
            font.pixelSize: 30 * ratio
            color: "#eceff4"
        }
        OptionsList {
            id: startDate
            optionName: defaultData["IUserTaskDateDataExtention/1.0"]["startDate"].getTime(
                            ) === date.getTime(
                            ) ? "None" : "Starts " + date.toLocaleString(
                                    locale, Locale.ShortFormat)
            Layout.preferredHeight: 60 * ratio
            Layout.fillWidth: true
            property var date: modelData.startDate

            optionsModel: ListModel {
                ListElement {
                    name: "Today"
                    isSpecial: false
                    plusDays: 0
                }
                ListElement {
                    name: "Tomorrow"
                    isSpecial: false
                    plusDays: 1
                }
                ListElement {
                    name: "Next week"
                    isSpecial: false
                    plusDays: 7
                }
                //                    ListElement {
                //                        name: "Specific date..."
                //                        isSpecial: true
                //                        plusDays: 0
                //                    }
            }

            Component.onCompleted: {
                optionPicked.connect(onOptionPicked)
            }
            function onOptionPicked(itemData) {
                if (itemData.isSpecial) {
                    datePicker.callback = pickerCallback
                    datePicker.open()
                } else {
                    modelData.startDate = new Date(new Date().getTime(
                                        ) + itemData.plusDays * 86400000)
                }
            }
            function pickerCallback() {
                modelData.startDate = datePicker.selectedDate
            }
        }

        Text {
            text: "Due date"
            font.pixelSize: 30 * ratio
            color: "#eceff4"
        }
        OptionsList {
            id: dueDate
            optionName: defaultData["IUserTaskDateDataExtention/1.0"]["dueDate"].getTime(
                            ) === date.getTime(
                            ) ? "None" : "Due " + date.toLocaleString(
                                    locale, Locale.ShortFormat)
            Layout.preferredHeight: 60 * ratio
            Layout.fillWidth: true
            property var date: modelData.dueDate

            optionsModel: ListModel {
                ListElement {
                    name: "Today"
                    isSpecial: false
                    plusDays: 0
                }
                ListElement {
                    name: "Tomorrow"
                    isSpecial: false
                    plusDays: 1
                }
                ListElement {
                    name: "Next week"
                    isSpecial: false
                    plusDays: 7
                }
                //                    ListElement {
                //                        name: "Specific date..."
                //                        isSpecial: true
                //                        plusDays: 0
                //                    }
            }

            Component.onCompleted: {
                optionPicked.connect(onOptionPicked)
            }
            function onOptionPicked(itemData) {
                if (itemData.isSpecial) {
                    datePicker.callback = pickerCallback
                    datePicker.open()
                } else {
                    modelData.dueDate = new Date(new Date().getTime(
                                        ) + itemData.plusDays * 86400000)
                }
            }
            function pickerCallback() {
                modelData.dueDate = datePicker.selectedDate
            }
        }
        Text {
            text: "Estimates"
            font.pixelSize: 30 * ratio
            color: "#eceff4"
        }
        OptionsList {
            id: timeCost
            optionName: defaultData["IUserTaskDateDataExtention/1.0"]["timeCost"].getTime(
                            ) === time.getTime(
                            ) ? "None" : "Estimated " + time.toLocaleTimeString(
                                    locale, Locale.ShortFormat)
            Layout.preferredHeight: 60 * ratio
            Layout.fillWidth: true
            property var time: modelData.timeCost

            optionsModel: ListModel {
                ListElement {
                    name: "5 minutes"
                    isSpecial: false
                    hours: 0
                    minutes: 5
                }
                ListElement {
                    name: "30 minutes"
                    isSpecial: false
                    hours: 0
                    minutes: 30
                }
                ListElement {
                    name: "1 hour"
                    isSpecial: false
                    hours: 1
                    minutes: 0
                }
                ListElement {
                    name: "Specific time..."
                    isSpecial: true
                    hours: 0
                    minutes: 0
                }
            }

            Component.onCompleted: {
                optionPicked.connect(onOptionPicked)
            }
            function onOptionPicked(itemData) {
                if (itemData.isSpecial) {
                    timePicker.callback = pickerCallback
                    timePicker.open()
                } else {
                    modelData.timeCost = Date.fromLocaleString(
                                locale,
                                itemData.hours + ":" + itemData.minutes, "h:m")
                }
            }
            function pickerCallback() {
                modelData.timeCost = Date.fromLocaleString(
                            locale,
                            timePicker.hrsDisplay + ":" + timePicker.minutesDisplay,
                            "h:m")
            }
        }
        Text {
            text: "Repeat"
            font.pixelSize: 30 * ratio
            color: "#eceff4"
        }
        OptionsList {
            id: repeatTypeOptions
            optionName: repeatType === 0 ? "None" : "Repeat " + optionsModel.get(repeatType - 1).name
            Layout.preferredHeight: 60 * ratio
            Layout.fillWidth: true
            property var repeatType: modelData.repeatType

            optionsModel: ListModel {
                ListElement {
                    name: "Daily"
                    type: 1
                }
                ListElement {
                    name: "Weekly"
                    type: 2
                }
                ListElement {
                    name: "Monthly"
                    type: 3
                }
                ListElement {
                    name: "Yearly"
                    type: 4
                }
            }

            Component.onCompleted: {
                optionPicked.connect(onOptionPicked)
            }
            function onOptionPicked(itemData) {
                modelData.repeatType = itemData.type
            }
        }

        Button {
            Layout.preferredWidth: 60 * ratio
            Layout.preferredHeight: Layout.preferredWidth
            Layout.alignment: Qt.AlignBottom | Qt.AlignLeft
            icon.source: "qrc:/Icons/back.png"
			icon.color: "#eceff4"
			icon.width: width / 2
			icon.height: icon.width
            background: Rectangle {
                color: parent.pressed ? "#88c0d0" : "#81a1c1"
                radius: 90
            }
            onClicked: {
                close()
            }
        }

        Button {
            Layout.preferredWidth: 60 * ratio
            Layout.preferredHeight: Layout.preferredWidth
            Layout.alignment: Qt.AlignBottom | Qt.AlignRight
			anchors.margins: 10 * ratio
			icon.source: "qrc:/Icons/ic_done_black_24dp.png"
			icon.color: "#eceff4"
			icon.width: width / 2
			icon.height: icon.width
            background: Rectangle {
                color: parent.pressed ? "#88c0d0" : "#81a1c1"
                radius: 90
            }
            onClicked: {
                console.log("Edit" + editingItemId + " " + taskName.isFilledCheck(
                                ))
                if (!taskName.isFilledCheck()) {
                    return
                }

                var taskData = {
                    "IUserTaskDataExtention/1.0": {
                        "name": taskName.text
                    },
                    "IUserTaskProjectDataExtention/1.0": {
                        "isProject": isProject
                    }
                }
                taskData["IUserTaskDateDataExtention/1.0"] = {
                    "startDate": startDate.date,
                    "dueDate": dueDate.date,
                    "timeCost": timeCost.time
                }
                if (editingItemId !== -1) {
                    console.log("taskData "
                                + taskData["IUserTaskDateDataExtention/1.0"]["startDate"])
                    taskModel.updateVariantItem(editingItemId, taskData)
                } else {
                    taskModel.addVariantItem(taskData, -1, taskModel.parentId)
                    taskName.clear()
                }
                close()
            }
        }
    }
}
