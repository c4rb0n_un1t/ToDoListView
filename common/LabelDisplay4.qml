// ekke (Ekkehard Gentz) @ekkescornerimport QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Label {
    Layout.fillWidth: true
    font.pixelSize: fontSizeDisplay4
    opacity: opacityDisplay4
}
