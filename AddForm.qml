import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "popups"

Rectangle {
    property var locale: Qt.locale()
    
    property int editingItemId: -1
    property var editingData: editingItemId !== -1 ? taskModel.getVariantItem(editingItemId) : null
	property var defaultData: taskModel.getVariantDefaultItem()
	property bool isProject: false
	signal onOpenedChanged(bool isOpened)

    function openAdd(isProject_) {
		editingItemId = -1
		isProject = isProject_
		taskName.placeholderText = isProject ? "Add project name..." : "Add task name..."
		
		dueDateOptions.date = null
		dueDateOptions.date = null
		timeCostOptions.time = null
		repeatTypeOptions.repeatType = null
        visible = true
        onOpenedChanged(true)
    }

    function close() {
		editingItemId = -1
		dueDateOptions.date = null
		dueDateOptions.date = null
		timeCostOptions.time = null
		repeatTypeOptions.repeatType = null
		isProject = false
        visible = false
        onOpenedChanged(false)
    }
    
    MouseArea {
		anchors.fill: parent
    }

    visible: false
    height: parent.height / 3
    color: "#3b4252"
    border.width: 1
    border.color: "#88c0d0"
    ColumnLayout {
        id: contentLayout
        anchors.fill: parent
        anchors.margins: 20 * ratio

        RowLayout {
            TextField {
                id: taskName
				property bool isInvalidated: false
                Layout.fillWidth: true
                placeholderText: "Add task..."
                text: editingData !== null ? editingData["IUserTaskDataExtention/1.0"]["name"] : ""
				font.pixelSize: 30 * ratio
                background: Rectangle {
                    color: "#d8dee9"
                    radius: height / 4
                }
                function isFilledCheck()
                {
					if(text === "")
					{
						background.color = "#bf616a"
						isInvalidated = true
						return false
					}
					return true
                }
                function reset()
                {
					taskName.background.color = "#d8dee9"
                }
                onTextChanged: {
					if(isInvalidated && text !== "") {
						isInvalidated = false
						reset()
					}
                }
            }
            Button {
                Layout.preferredWidth: 60 * ratio
                Layout.preferredHeight: Layout.preferredWidth
                icon.source: "qrc:/Icons/ic_done_black_24dp.png"
                icon.color: "white"
                icon.width: Layout.preferredWidth * 0.66
                icon.height: icon.width
                background: Rectangle {
                    color: parent.pressed ? "#88c0d0" : "#81a1c1"
                    radius: 90
                }
                onClicked: {
					if(!taskName.isFilledCheck())
					{
						return
					}

                    var taskData = {
                        "IUserTaskDataExtention/1.0": {
                            "name": taskName.text
                        },
                        "IUserTaskProjectDataExtention/1.0": {
							"isProject": isProject
                        },
						
                    }
                    
                    if(repeatTypeOptions.repeatType !== null)
                    {
						taskData["IUserTaskProjectRepeatExtention/1.0"] = {
							"repeatType": repeatTypeOptions.repeatType
						}
                    }
					
					taskData["IUserTaskDateDataExtention/1.0"] = {}
					if(startDateOptions.date !== null)
					{
						taskData["IUserTaskDateDataExtention/1.0"]["startDate"] = startDateOptions.date
					}
					if(dueDateOptions.date !== null)
					{
						taskData["IUserTaskDateDataExtention/1.0"]["dueDate"] = dueDateOptions.date
					}
					if(timeCostOptions.time !== null)
					{
						taskData["IUserTaskDateDataExtention/1.0"]["timeCost"] = timeCostOptions.date
					}
                    if(editingItemId !== -1) {
						taskModel.updateVariantItem(editingItemId, taskData)
                    }
                    else {
						taskModel.addVariantItem(taskData, -1, taskModel.parentId)
						taskName.clear()
                    }
                    close()
                }
            }
        }

        ScrollView {
            Layout.fillWidth: true
            
            RowLayout {
				id: optionsRow
				OptionsList {
					id: startDateOptions
					optionName: date === null || defaultData["IUserTaskDateDataExtention/1.0"]["startDate"].getTime() === date.getTime() ?
						"Start date" : "Starts " + date.toLocaleString(locale, Locale.ShortFormat)
					Layout.preferredHeight: 60 * ratio
					property var date: null
	
					optionsModel: ListModel {
						ListElement {
							name: "Today"
							isSpecial: false
							plusDays: 0
						}
						ListElement {
							name: "Tomorrow"
							isSpecial: false
							plusDays: 1
						}
						ListElement {
							name: "Next week"
							isSpecial: false
							plusDays: 7
						}
	//                    ListElement {
	//                        name: "Specific date..."
	//                        isSpecial: true
	//                        plusDays: 0
	//                    }
					}
	
					Component.onCompleted: {
						optionPicked.connect(onOptionPicked)
					}
					function onOptionPicked(itemData) {
						if (itemData.isSpecial) {
							datePicker.callback = () => {
								date = datePicker.selectedDate
							}
							datePicker.open()
						} else {
							date = new Date(new Date().getTime() + itemData.plusDays * 86400000)
						}
					}
				}
				
				OptionsList {
					id: dueDateOptions
					optionName: date === null || defaultData["IUserTaskDateDataExtention/1.0"]["dueDate"].getTime() === date.getTime() ?
						"Due date" : "Due " + date.toLocaleString(locale, Locale.ShortFormat)
					Layout.preferredHeight: 60 * ratio
					property var date: null
	
					optionsModel: ListModel {
						ListElement {
							name: "Today"
							isSpecial: false
							plusDays: 0
						}
						ListElement {
							name: "Tomorrow"
							isSpecial: false
							plusDays: 1
						}
						ListElement {
							name: "Next week"
							isSpecial: false
							plusDays: 7
						}
	//                    ListElement {
	//                        name: "Specific date..."
	//                        isSpecial: true
	//                        plusDays: 0
	//                    }
					}
	
					Component.onCompleted: {
						optionPicked.connect(onOptionPicked)
					}
					function onOptionPicked(itemData) {
						if (itemData.isSpecial) {
							datePicker.callback = () => {
								date = datePicker.selectedDate
							}
							datePicker.open()
						} else {
							date = new Date(new Date().getTime() + itemData.plusDays * 86400000)
						}
					}
				}
	
				OptionsList {
					id: timeCostOptions
					optionName: time === null || defaultData["IUserTaskDateDataExtention/1.0"]["timeCost"].getTime() === time.getTime() ?
						"Estimates" : "Estimated "+ time.toLocaleTimeString(locale, Locale.ShortFormat)
					Layout.preferredHeight: 60 * ratio
					property var time: null
	
					optionsModel: ListModel {
						ListElement {
							name: "5 minutes"
							isSpecial: false
							hours: 0
							minutes: 5
						}
						ListElement {
							name: "30 minutes"
							isSpecial: false
							hours: 0
							minutes: 30
						}
						ListElement {
							name: "1 hour"
							isSpecial: false
							hours: 1
							minutes: 0
						}
						ListElement {
							name: "Specific time..."
							isSpecial: true
							hours: 0
							minutes: 0
						}
					}
	
					Component.onCompleted: {
						optionPicked.connect(onOptionPicked)
					}
					function onOptionPicked(itemData) {
						if (itemData.isSpecial) {
							timePicker.callback = () => {
								time = Date.fromLocaleString(locale, timePicker.hrsDisplay + ":" + timePicker.minutesDisplay, "h:m")
							}
							timePicker.open()
						} else {
							time = Date.fromLocaleString(locale, itemData.hours + ":" + itemData.minutes, "h:m")
						}
					}
				}
	
				OptionsList {
					id: repeatTypeOptions
					optionName: repeatType === null || repeatType === 0 ?
						"Repeat" : "Repeat "+ optionsModel.get(repeatType-1).name
					Layout.preferredHeight: 60 * ratio
					property var repeatType: null
	
					optionsModel: ListModel {
						ListElement {
							name: "Daily"
							type: 1
						}
						ListElement {
							name: "Weekly"
							type: 2
						}
						ListElement {
							name: "Monthly"
							type: 3
						}
						ListElement {
							name: "Yearly"
							type: 4
						}
					}
	
					Component.onCompleted: {
						optionPicked.connect(onOptionPicked)
					}
					function onOptionPicked(itemData) {
						repeatType = itemData.type
					}
				}
            }
        }

        Button {
            Layout.preferredWidth: 60 * ratio
            Layout.preferredHeight: Layout.preferredWidth
            Layout.alignment: Qt.AlignBottom | Qt.AlignLeft
            icon.source: "qrc:/Icons/back.png"
            icon.color: "white"
            background: Rectangle {
                color: parent.pressed ? "#88c0d0" : "#81a1c1"
                radius: 90
            }
            onClicked: {
                close()
            }
        }
    }
}
