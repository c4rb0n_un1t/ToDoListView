#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this)
	, m_GUIElementBase(new GUIElementBase(this, {"MainMenuItem"}, "qrc:/ToDoListView.qml"))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this},
		{INTERFACE(IGUIElement), m_GUIElementBase}
	},
	{
		{INTERFACE(IUserTaskDataExtention), m_taskManager},
		{INTERFACE(IUserTaskDateDataExtention), m_dateTaskManager},
		{INTERFACE(IUserTaskProjectDataExtention), m_projectTaskManager},
	},
	{});
	m_GUIElementBase->initGUIElementBase();
}

Plugin::~Plugin()
{
}

void Plugin::onReady()
{
	m_userTasksFilter = m_taskManager->getModel()->getFilter();
	m_GUIElementBase->rootContext()->setContextProperty("taskModel", m_userTasksFilter);
	connect(m_GUIElementBase->rootObject(), SIGNAL(onIndexSelected(int)), this, SLOT(onIndexSelected(int)));
}

void Plugin::onIndexSelected(int index)
{
	m_userTasksFilter->setTreeParent(index);
}
